.. neatlog documentation master file, created by
   sphinx-quickstart on Fri Jan 29 14:28:29 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

#####################################
neatlog: A neat logging configuration
#####################################

.. image:: https://img.shields.io/pypi/v/neatlog?style=flat-square
   :target: https://pypi.org/project/neatlog/
   :alt: PyPI

.. image:: https://img.shields.io/pypi/l/neatlog?style=flat-square
   :target: https://gitlab.com/szs/neatlog/-/raw/master/LICENSE
   :alt: PyPI - License

.. image:: https://img.shields.io/pypi/pyversions/neatlog?style=flat-square
   :target: https://python.org
   :alt: PyPI - Python Version

.. image:: https://img.shields.io/gitlab/pipeline/szs/neatlog?style=flat-square
   :target: https://gitlab.com/szs/neatlog/-/pipelines
   :alt: Gitlab pipeline status

.. image:: https://gitlab.com/szs/neatlog/badges/master/coverage.svg?style=flat-square
   :target: https://gitlab.com/szs/neatlog/-/pipelines
   :alt: Coverage

.. image:: https://readthedocs.org/projects/neatlog/badge/?version=latest
   :target: https://neatlog.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status

This package provides an easy and transparent way of customizing the builtin logging.
Just import the module and enjoy the difference.

.. contents:: Table of Contents

Quick start
===========

Install by typing

.. code-block:: shell

                pip install neatlog

and simply import neatlog in your program and use logging as usually:

.. code-block:: python

                import logging
                import neatlog

                logging.critical("something critical")
                logging.error("some error")
                logging.warning("some warning")

.. image:: _static/screenshot_03.png

Or use the ``log`` decorator to log function calls:

.. code-block:: python

                import logging
                from time import sleep
                from neatlog import log, set_log_level

                set_log_level(logging.DEBUG)

                @log
                def foo(x):
                    sleep(1)
                    logging.info(f"in foo, arg: {x}")

                foo(2)

.. image:: _static/screenshot_04.png

Why not use the built in logging?
=================================

In fact, neatlog uses Python's built in logging, so let's rephrase the question:
**"Why use neatlog on top of logging?"**

If you have ever found yourself looking up how to configure the logging format
for the 195th time in order to get a more appealing or more parsable output, or
if copying the ``basic_config`` command from another project over to the new one,
then you may appreciate to get a neat and very readable logging setup by simply
importing neatlog.

At the same time, neatlog is totally transparent, meaning it merely provides, configures,
adds and removes `handlers <https://docs.python.org/3/library/logging.html#handler-objects>`_
and `formatters <https://docs.python.org/3/library/logging.html#formatter-objects>`_.
So you can combine neatlog's configuration with your own handlers or access the
handlers and formatters to fiddle around with them.


Documentation
=============

.. toctree::
   :maxdepth: 2

   installation
   usage


How to Contribute
=================

If you find a bug, want to propose a feature or need help getting this package to work with your data
on your system, please don't hesitate to file an `issue <https://gitlab.com/szs/neatlog/-/issues>`_ or write
an email. Merge requests are also much appreciated!

Project links
=============

* `Repository <https://gitlab.com/szs/neatlog>`_
* `Documentation <https://neatlog.readthedocs.io/en/latest/>`_
* `pypi page <https://pypi.org/project/neatlog/>`_
