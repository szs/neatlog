Usage
=====

Simply import neatlog in your program and use logging as usually:

.. code-block:: python

                import logging
                from log_fmt import set_log_level

                set_log_level(logging.DEBUG)
                logging.critical("something critical")
                logging.error("some error")
                logging.warning("some warning")
                logging.info("some info")
                logging.debug("something for debugging")

Configuration
=============

tbd.

.. code-block:: python

                import logging
                from log_fmt import set_log_level

                set_log_level(logging.DEBUG)
                logging.critical("something critical")
                logging.error("some error")
                logging.warning("some warning")
                logging.info("some info")
                logging.debug("something for debugging")



.. code-block:: python

                import logging
                from log_fmt import config_logger

                # switch on logging debug info to a file
                config_logger(log_to_file=True, filename="debug.log", level=logging.DEBUG)

                logging.critical("something critical")
                logging.error("some error")
                logging.warning("some warning")
                logging.info("some info")
                logging.debug("something for debugging")
