import logging

from neatlog import config_logger, log, set_log_level


def test_config_logger_compact(caplog):
    config_logger(log_to_stream=True, compact=True, color=True)
    logging.critical("something critical")
    logging.error("some error")
    logging.warning("some warning")
    logging.info("some info")
    logging.debug("something for debugging")
    # print([rec.message for rec in caplog.records])
    assert [
        "something critical",
        "some error",
        "some warning",
        "some info",
        "something for debugging",
    ] == [rec.message for rec in caplog.records]


def test_config_logger_log_to_stream_false():
    config_logger(log_to_stream=False)
    assert not any(
        (type(h) == logging.StreamHandler) for h in logging.getLogger().handlers
    )


def test_config_logger_log_to_file_true(tmp_path):
    config_logger(log_to_file=True, filename=tmp_path / "log")
    assert any((type(h) == logging.FileHandler) for h in logging.getLogger().handlers)


def test_log_level_default(caplog):
    logging.critical("something critical")
    logging.error("some error")
    logging.warning("some warning")
    logging.info("some info")
    logging.debug("something for debugging")
    # print([rec.message for rec in caplog.records])
    assert [
        "something critical",
        "some error",
        "some warning",
        "some info",
        "something for debugging",
    ] == [rec.message for rec in caplog.records]


def test_log_level_debug(caplog):
    # with info log level
    set_log_level(logging.DEBUG)
    logging.critical("something critical")
    logging.error("some error")
    logging.warning("some warning")
    logging.info("some info")
    logging.debug("something for debugging")
    # print([rec.message for rec in caplog.records])
    assert [
        "something critical",
        "some error",
        "some warning",
        "some info",
        "something for debugging",
    ] == [rec.message for rec in caplog.records]


def test_log_level_info(caplog):
    # with info log level
    set_log_level(logging.INFO)
    logging.critical("something critical")
    logging.error("some error")
    logging.warning("some warning")
    logging.info("some info")
    logging.debug("something for debugging")
    # print([rec.message for rec in caplog.records])
    assert ["something critical", "some error", "some warning", "some info"] == [
        rec.message for rec in caplog.records
    ]


def test_log_level_warning(caplog):
    # with info log level
    set_log_level(logging.WARNING)
    logging.critical("something critical")
    logging.error("some error")
    logging.warning("some warning")
    logging.info("some info")
    logging.debug("something for debugging")
    # print([rec.message for rec in caplog.records])
    assert ["something critical", "some error", "some warning"] == [
        rec.message for rec in caplog.records
    ]


def test_log_level_error(caplog):
    # with info log level
    set_log_level(logging.ERROR)
    logging.critical("something critical")
    logging.error("some error")
    logging.warning("some warning")
    logging.info("some info")
    logging.debug("something for debugging")
    # print([rec.message for rec in caplog.records])
    assert ["something critical", "some error"] == [
        rec.message for rec in caplog.records
    ]


def test_log_level_critical(caplog):
    # with info log level
    set_log_level(logging.CRITICAL)
    logging.critical("something critical")
    logging.error("some error")
    logging.warning("some warning")
    logging.info("some info")
    logging.debug("something for debugging")
    # print([rec.message for rec in caplog.records])
    assert ["something critical"] == [rec.message for rec in caplog.records]


def test_config_logger_fmt(caplog):
    config_logger(
        log_to_stream=True,
        fmt="%(asctime)s - %(name)s - %(levelname)s- %(message)s",
        level=logging.DEBUG,
        color=True,
    )
    set_log_level(
        logging.DEBUG
    )  # this is needed because of the caplog handlers.... I believe

    logging.critical("something critical")
    logging.error("some error")
    logging.warning("some warning")
    logging.info("some info")
    logging.debug("something for debugging")
    # print([rec.message for rec in caplog.records])
    assert [
        "something critical",
        "some error",
        "some warning",
        "some info",
        "something for debugging",
    ] == [rec.message for rec in caplog.records]


def test_config_logger_prefix_suffix_color(caplog):
    config_logger(
        log_to_stream=True, prefix="«", suffix="»", level=logging.INFO, color=False
    )
    logging.info("some info")
    assert ["some info"] == [rec.message for rec in caplog.records]


def test_decorator(caplog):
    set_log_level(logging.DEBUG)

    @log
    def foo1():
        pass

    foo1()

    assert len(caplog.records) == 1
    assert caplog.records[0].message.startswith("call 1 of foo1() returned None")
    assert caplog.records[0].levelname == "DEBUG"

    @log()
    def foo2():
        pass

    foo2()

    assert len(caplog.records) == 2
    assert caplog.records[-1].message.startswith("call 1 of foo2() returned None")
    assert caplog.records[-1].levelname == "DEBUG"

    @log(30)
    def foo3():
        pass

    foo3()

    assert len(caplog.records) == 3
    assert caplog.records[-1].message.startswith("call 1 of foo3() returned None")

    @log(level=30)
    def foo4():
        pass

    foo4()

    assert len(caplog.records) == 4
    assert caplog.records[-1].message.startswith("call 1 of foo4() returned None")
    assert caplog.records[-1].levelname == "WARNING"

    @log
    def foo5():
        pass

    foo5()

    assert len(caplog.records) == 5
    assert caplog.records[-1].message.startswith("call 1 of foo5() returned None")
    assert caplog.records[-1].levelname == "DEBUG"

    @log()
    def foo6():
        pass

    foo6()

    assert len(caplog.records) == 6
    assert caplog.records[-1].message.startswith("call 1 of foo6() returned None")
    assert caplog.records[-1].levelname == "DEBUG"

    @log(30)
    def foo7():
        pass

    foo7()

    assert len(caplog.records) == 7
    assert caplog.records[-1].message.startswith("call 1 of foo7() returned None")
    assert caplog.records[-1].levelname == "WARNING"

    @log(level=30)
    def foo8():
        pass

    foo8()

    assert len(caplog.records) == 8
    assert caplog.records[-1].message.startswith("call 1 of foo8() returned None")
    assert caplog.records[-1].levelname == "WARNING"
